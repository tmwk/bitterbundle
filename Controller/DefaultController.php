<?php

namespace Tmwk\BitterBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class DefaultController
 * @package Tmwk\BitterBundle\Controller
 * @Route("/bitter")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="bitter")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {



        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ));
    }
}
