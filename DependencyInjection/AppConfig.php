<?php
/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 27/12/2016
 * Time: 14:39
 */

namespace Tmwk\BitterBundle\DependencyInjection;


use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class AppConfig
 * @package Tmwk\BitterBundle\DependencyInjection
 */
class AppConfig
{
    private $_em;
    private $_container;
    private $_request;


    /**
     * AppConfig constructor.
     * @param EntityManager $em
     * @param ContainerInterface $container
     */
    public function __construct(EntityManager $em, ContainerInterface $container, RequestStack $requestStack)
    {
        $this->_em        = $em;
        $this->_container = $container;
        $this->_request   = $requestStack;
    }

    /**
     * @return bool
     */
    public function hasAnalytics()
    {
        if ($this->_container->getParameter('has_analytics') == 'true') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function isAjax()
    {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') || isset($_GET['is_ajax']);
    }

    /**
     * @return mixed
     */
    public function getFacebookAppId()
    {
        return $this->getParameter('facebook.' . $this->getEnvironment() . '.app_id');
    }

    /**
     * @return string
     */
    public function getCurrentUrl()
    {
        return $this->getRequest()->getUri();
    }

    /**
     * @param $param
     * @return mixed
     */
    public function getParameter($param)
    {
        return $this->_container->getParameter($param);
    }

    /**
     * @return mixed
     */
    public function getEnvironment()
    {
        return $this->getParameter('kernel.environment');
    }

    /**
     * @return null|Request
     */
    protected function getRequest()
    {
        return $this->_request->getCurrentRequest();
    }


}