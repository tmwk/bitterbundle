<?php

namespace Tmwk\BitterBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('tmwk_bitter')
            ->children()
                ->arrayNode('facebook')
                    ->isRequired()
                    ->children()
                        ->arrayNode('prod')->isRequired()
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('app_id')->isRequired()->end()
                                ->scalarNode('app_secret')->isRequired()->end()
                            ->end()
                        ->end()
                        ->arrayNode('dev')->isRequired()
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('app_id')->isRequired()->end()
                                ->scalarNode('app_secret')->isRequired()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('app')
                    ->isRequired()
                    ->children()
                        ->scalarNode('facebook_admin')->isRequired()->end()
                        ->scalarNode('has_analytics')->isRequired()->end()
                        ->scalarNode('assets_version')->isRequired()->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
