<?php
/**
 * Created by PhpStorm.
 * User: Mario
 * Date: 29/03/2016
 * Time: 14:42
 */

namespace Tmwk\BitterBundle\Utilities;

use Symfony\Component\HttpFoundation\Request;

class Session
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function set($slug, $name, $value)
    {
        $session     = $this->request->getSession();
        $item        = $session->get($slug, array());
        $item[$name] = $value;
        $session->set($slug, $item);
    }

    public function get($slug, $name = false)
    {
        $session = $this->request->getSession();

        if ($session->has($slug)) {
            $item = $session->get($slug);
            if (!$name) {
                return $item;
            }
            if (isset($item[$name])) {
                return $item[$name];
            }
        }
        return false;
    }

    public function has($slug, $value = false)
    {
        $session = $this->request->getSession();

        if ($session->has($slug)) {
            $item = $session->get($slug);
            if (!$value) {
                return true;
            }
            if (isset($item[$value])) {
                return true;
            }
        }

        return false;
    }

    public function remove($slug)
    {
        $session = $this->request->getSession();

        if ($session->has($slug)) {
            $session->remove($slug);
            return true;
        }

        return false;
    }
}