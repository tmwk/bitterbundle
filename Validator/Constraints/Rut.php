<?php

/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 23/11/2016
 * Time: 13:16
 */
namespace Tmwk\BitterBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Rut extends Constraint
{
    public $message = 'El rut %string% no es valido.';

    public function validatedBy()
    {
        return 'rut.validator';
    }
}