<?php

/**
 * Created by PhpStorm.
 * User: mfigueroa
 * Date: 24/11/2016
 * Time: 11:22
 */
namespace Tmwk\BitterBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CheckUserInformation
{
    private $em;
    private $container;

    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em   = $em;
        $this->container = $container;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $request   = $this->container->get('request');
        $routeName = $request->get('_route');
        
        if($routeName !== 'close' and $this->container->getParameter('close') == true){
            $event->setController(function () {
                return new RedirectResponse($this->generateUrl('close'));
            });
        }

        $token = $this->container->get('security.token_storage');
        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            if ($token->getToken()->getUser()->getIsUpdate() == false) {
                if($routeName !== 'form' && $routeName !== '_profiler' && $routeName !== 'homepage' && $routeName !== '_wdt' && $routeName !== NULL){
                    $event->setController(function () {
                        return new RedirectResponse($this->generateUrl('form'));
                    });
                }
            }
        }
        return false;
    }

    public function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }
}